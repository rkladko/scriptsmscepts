#!/usr/bin/env bash

./codecept.sh run --fail-fast -vv functional "$@"