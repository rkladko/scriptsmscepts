<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = NULL)
 *
 * @SuppressWarnings(PHPMD)
*/
class FunctionalTester extends \Codeception\Actor
{
    use _generated\FunctionalTesterActions;

    protected $_developer = 'Unknown';
    protected $_script;
    protected $_needActivation;
    protected $_profileId;
    protected $_login;
    protected $_password;
    protected $_email;
    protected $_actlink;

    protected function stepMessage($step)
    {
        $methodName = strtoupper(preg_replace('/([a-z])([A-Z])/', '$1_$2', explode('::', $step)[1]));
        $messageConst = sprintf('static::MSG_%s', $methodName);
        return sprintf($$messageConst, $this->_script);
    }

    protected function composeRegisterUrl()
    {
        if ($this->scenario->current('env') === 'local') {
            return '/test-script.html';
        }
        return sprintf('/test-script?code=%s&action=register&developer=%s',
            $this->_script, $this->_developer
        );
    }

    protected function composeReadEmailUrl()
    {
        if ($this->scenario->current('env') === 'local') {
            return '/read-email.html';
        }
        return sprintf('/read-email?developer=%s&code=%s&profileId=%s',
            $this->_developer, $this->_script, $this->_profileId
        );
    }

    protected function composeActivateUrl()
    {
        $keys = ['action', 'developer', 'code', 'profileId', 'actlink', 'login', 'password', 'email'];
        $values = ['activate', $this->_developer, $this->_script, $this->_profileId, $this->_actlink,
            $this->_login, $this->_password, urlencode($this->_email)
        ];
        $queryString = implode('&', array_map(function($k, $v) {
            return "$k=$v";
        }, $keys, $values));

        $url = "http://msphp.uk.to/web/developers/test-script?$queryString";
        codecept_debug("Activate url: $url");
        return $url;
        /*
         * http://msphp.uk.to/web/developers/test-script?
         * developer=ruslan_kladko
         * &code=BookmarkingServices/bibsonomy
         * &profileId=243
         * &action=activate&
         * actlink=http%3A%2F%2Fwww.bibsonomy.org%2Factivate%2F024aed7be36576443a825628e50791a5&email=deena_lauriano66%40inbox.lv
         * &login=havenabel1985
         * &password=PL04@_nhcc
         */
    }

    protected function grabRegisteredParams()
    {
        $this->_profileId = $this->grabTextFrom('~Profile Id:\s+(\d+)~');
        codecept_debug('> ' . $this->_profileId);
        $this->_login = $this->grabTextFrom("~'Login' => '(.+)'~");
        codecept_debug('> ' . $this->_login);
        $this->_password = $this->grabTextFrom("~'Password' => '(.+)'~");
        codecept_debug('> ' . $this->_password);
        $this->_email = $this->grabTextFrom("~'Email' => '(.+)'~");
        codecept_debug('> ' . $this->_email);
    }

    protected function grabActLink()
    {
        $container = $this->grabTextFrom('//div[@class="container"]/pre[6]');
        preg_match('/\s(.+)$/', $container, $matches);
        return $matches[1];
    }

    protected function registerAccount()
    {
        $i = $this;
        $i->wantTo('register account on ' . $this->_script);
        $i->amOnPage($this->composeRegisterUrl());
        $i->dontSee('with status error');
        if ($this->_needActivation) {
            $i->see('with status waiting activation');
        } else {
            $i->see('with status complete');
        }
        $i->see('Profile Id');
        $i->grabRegisteredParams();
    }

    /**
     * @depends registerAccount
     */
    protected function readEmail()
    {
        $i = $this;
        $i->wantTo('read activation email');
        $i->amOnPage($this->composeReadEmailUrl());
        $i->dontSee('Unable to connect to email server');
        $i->see('Activation link found');
        $this->_actlink = $this->grabActLink();
        codecept_debug("ACTLINK: {$this->_actlink}");
    }

    /**
     * @depends readEmail
     */
    protected function activateAccount()
    {
        $i = $this;
        $i->wantTo('activate account on ' . $this->_script);
        $i->amOnPage($this->composeActivateUrl());
        $i->see('with status complete');
    }

    public function wantToTestScript($script, $developer, $needActivation = true)
    {
        $this->_script = $script;
        $this->_developer = $developer;
        $this->_needActivation = $needActivation;

        $this->registerAccount();
        if ($needActivation) {
            $this->readEmail();
            $this->activateAccount();
        }
    }
}
